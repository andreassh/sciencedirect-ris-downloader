This is a method for exporting more than 100 titles and abstracts at once from ScienceDirect (even when not logged in) and downloading the combined RIS file.

The maximum number of records that can be downloaded when not logged in or logged in with a regular user account is 1000. However, when logged in with institutional access, the maximum number of records that can be downloaded should be 6000.


## 1. Conduct your search
Open [ScienceDirect](https://www.sciencedirect.com) and conduct your search. Make any filtering choices, if needed and stay in the first results page.

## 2. Open browser's Console
Open your browser's Console. Instructions for different browsers are as follows:

**Chrome**
* Open Chrome on your computer.
* Press the Ctrl+Shift+J keys on Windows or Cmd+Option+J keys on Mac to open the developer console.
* Alternatively, right-click anywhere on the webpage and select "Inspect" to open the developer console.

**Firefox**
* Open Firefox on your computer.
* Press the Ctrl+Shift+K keys on Windows or Cmd+Option+K keys on Mac to open the developer console.
* Alternatively, right-click anywhere on the webpage and select "Inspect Element" to open the developer console.

**Safari**
* Open Safari on your computer.
* Go to the "Safari" menu at the top left of your screen.
* Select "Preferences" or "Settings".
* Click on the "Advanced" tab.
* Check the box next to "Show Develop menu in menu bar."
* Go to the "Develop" menu in the menu bar.
* Select "Show JavaScript Console."

**Edge**
* Open Edge on your computer.
* Press the F12 key to open the developer tools.
* Alternatively, right-click anywhere on the webpage and select "Inspect Element" to open the developer tools.

**Brave**
* Open Brave on your computer.
* Press the Ctrl+Shift+I keys on Windows or Cmd+Option+I keys on Mac to open the developer tools.
* Alternatively, right-click anywhere on the webpage and select "Inspect Element" to open the developer tools.

**Vivaldi**
* Open Vivaldi on your computer.
* Press the Ctrl+Shift+I keys on Windows or Cmd+Option+I keys on Mac to open the developer tools.
* Alternatively, right-click anywhere on the webpage and select "Inspect" to open the developer tools.

**Opera**
* Open Opera on your computer.
* Press the Ctrl+Shift+I keys on Windows or Cmd+Option+I keys on Mac to open the developer tools.
* Alternatively, right-click anywhere on the webpage and select "Inspect Element" to open the developer tools.

## 3. Download records
Copy the following text, paste it into the browser's Console, and then press Enter to initiate the download.

    function _0x4923(){const _0x4ead46=['maxOffset','length','POST','none','forEach','803nJUFgM','replace','text','444606mWpkUv','searchToken','getElementById','2599926eyBQeQ','AL_ST','/search/api/export-citations','body','Alright,\x20let\x27s\x20try\x20to\x20do\x20this...\x20might\x20take\x20couple\x20of\x20seconds.','script','json','1168769mLZAtz','substr','5RijlnJ','46790ZXmZoj','values','2628rRVLQI','display','setAttribute','1584099GJgZRE','447100QeQoTb','targetUrl','pii','value','ris','href','click','getElementsByClassName','appendChild','\x20results','/search/api?','getElementsByTagName','download','token','push','10sbZxaQ','ceil','search-body-results-text','40FcQUFr','undefined','sciencedirect.ris','application/json'];_0x4923=function(){return _0x4ead46;};return _0x4923();}(function(_0x3fad30,_0x2e112b){const _0x401319=_0x4cbe,_0x25f644=_0x3fad30();while(!![]){try{const _0xb7fe84=parseInt(_0x401319(0x197))/0x1*(-parseInt(_0x401319(0x1aa))/0x2)+-parseInt(_0x401319(0x18b))/0x3+-parseInt(_0x401319(0x19b))/0x4+-parseInt(_0x401319(0x194))/0x5*(-parseInt(_0x401319(0x188))/0x6)+parseInt(_0x401319(0x192))/0x7*(parseInt(_0x401319(0x1ad))/0x8)+parseInt(_0x401319(0x19a))/0x9+-parseInt(_0x401319(0x195))/0xa*(-parseInt(_0x401319(0x185))/0xb);if(_0xb7fe84===_0x2e112b)break;else _0x25f644['push'](_0x25f644['shift']());}catch(_0x5e6c9a){_0x25f644['push'](_0x25f644['shift']());}}}(_0x4923,0x6a30d));function dl(_0x1ae343){const _0x442f65=_0x4cbe;var _0x5677f2=document['createElement']('a');_0x5677f2[_0x442f65(0x199)](_0x442f65(0x1a0),'data:text/plain;charset=utf-8,'+encodeURIComponent(_0x1ae343)),_0x5677f2['setAttribute'](_0x442f65(0x1a7),_0x442f65(0x1af)),_0x5677f2['style'][_0x442f65(0x198)]=_0x442f65(0x1b4),document[_0x442f65(0x18e)][_0x442f65(0x1a3)](_0x5677f2),_0x5677f2[_0x442f65(0x1a1)](),document['body']['removeChild'](_0x5677f2);}async function gR(){const _0x175826=_0x4cbe;alert(_0x175826(0x18f));try{let _0x106377=document[_0x175826(0x1a6)](_0x175826(0x190)),_0x5cd0c0='';Object[_0x175826(0x196)](_0x106377)[_0x175826(0x184)](function(_0x2b7780){const _0x573d2a=_0x175826;if(_0x2b7780[_0x573d2a(0x187)][_0x573d2a(0x193)](0x9,0x5)==_0x573d2a(0x18c))_0x5cd0c0=_0x2b7780[_0x573d2a(0x187)];});let _0x494a6d=_0x5cd0c0[_0x175826(0x193)](0x14,_0x5cd0c0['length']-0x1);_0x494a6d=_0x494a6d['replace'](_0x175826(0x1ae),'\x22undefined\x22');let _0x4083f1=JSON['parse'](_0x494a6d),_0x2e4426=_0x4083f1[_0x175826(0x1a8)][_0x175826(0x189)],_0x3b3782=document[_0x175826(0x18a)](_0x175826(0x19c))[_0x175826(0x19e)];_0x3b3782=_0x3b3782[_0x175826(0x186)]('/search?',_0x175826(0x1a5)),_0x3b3782=_0x3b3782['replace'](/&offset=\d*/gm,'');let _0x180f58=_0x3b3782+'&t='+_0x2e4426,_0x29a42f=_0x4083f1['auth']['searchExperience'][_0x175826(0x1b1)],_0xb01cfb=document[_0x175826(0x1a2)](_0x175826(0x1ac))[0x0]['textContent'];_0xb01cfb=_0xb01cfb['replace'](',',''),_0xb01cfb=_0xb01cfb[_0x175826(0x186)](_0x175826(0x1a4),''),_0xb01cfb=parseInt(_0xb01cfb);let _0x3d364e=Math[_0x175826(0x1ab)]((_0xb01cfb<_0x29a42f?_0xb01cfb:_0x29a42f)/0x64);var _0x1ffbb4=[];for(let _0x25f6be=0x0;_0x25f6be<_0x3d364e;_0x25f6be++){let _0xd2615e=_0x25f6be>0x0?_0x25f6be*0x64:'',_0x56357c=_0xd2615e?_0x180f58+'&offset='+_0xd2615e:_0x180f58,_0x530a44=await fetch(_0x56357c),_0x1f8256=await _0x530a44[_0x175826(0x191)]();_0x1f8256['searchResults'][_0x175826(0x184)](function(_0x3608f2){const _0x1dd830=_0x175826;_0x1ffbb4[_0x1dd830(0x1a9)](_0x3608f2[_0x1dd830(0x19d)]);});}var _0x5332e0='';for(let _0x203834=0x0;_0x203834<_0x1ffbb4[_0x175826(0x1b2)]/0x64;_0x203834++){let _0x1602f1=_0x203834==0x0?0x0:_0x203834*0x64+0x1,_0x5537f5=_0x1602f1+0x64;_0x5537f5=_0x1ffbb4[_0x175826(0x1b2)]<_0x5537f5?_0x1ffbb4[_0x175826(0x1b2)]:_0x5537f5;let _0x5c95c9=_0x1ffbb4['slice'](_0x1602f1,_0x5537f5),_0x42a8b6=await fetch(_0x175826(0x18d),{'method':_0x175826(0x1b3),'headers':{'Content-type':_0x175826(0x1b0)},'body':JSON['stringify']({'type':_0x175826(0x19f),'t':_0x2e4426,'pii':_0x5c95c9})}),_0x2a2dc9=await _0x42a8b6[_0x175826(0x187)]();_0x5332e0+=_0x2a2dc9+'\x0a';}return _0x5332e0;}catch(_0x187b2e){alert(_0x187b2e);}}function _0x4cbe(_0x4cb9b1,_0x3974e7){const _0x492348=_0x4923();return _0x4cbe=function(_0x4cbe3e,_0x48e7ad){_0x4cbe3e=_0x4cbe3e-0x184;let _0x39bf9f=_0x492348[_0x4cbe3e];return _0x39bf9f;},_0x4cbe(_0x4cb9b1,_0x3974e7);}gR()['then'](dl);
